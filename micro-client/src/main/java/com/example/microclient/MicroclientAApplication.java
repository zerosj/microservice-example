package com.example.microclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.example.microclient.remote")
@SpringBootApplication
public class MicroclientAApplication {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","a");
        SpringApplication.run(MicroclientAApplication.class, args);
    }
}
