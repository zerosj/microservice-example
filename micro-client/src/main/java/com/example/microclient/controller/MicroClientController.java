package com.example.microclient.controller;

import com.example.microclient.remote.MicroServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/7 17:27
 */

@RestController
public class MicroClientController {
    @Autowired
    private MicroServer microServer;

    @GetMapping(value = "/hello/{name}")
    public String hello (@PathVariable String name){
        return "hello"+  name;
    }

    @GetMapping(value = "/hello")
    public String  hello(){
        return microServer.hello();
    }
}
