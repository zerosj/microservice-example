package com.example.microclient.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "micro-server")
public interface MicroServer {
    @GetMapping(value = "/hello")
    String hello ();
}
