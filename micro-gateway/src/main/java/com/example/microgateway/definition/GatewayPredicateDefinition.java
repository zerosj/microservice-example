package com.example.microgateway.definition;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author lenovo
 * 网关断言的定义信息
 */

@Data
public class GatewayPredicateDefinition {
    private String name;
    private Map<String,String> args = new LinkedHashMap<>();
}
