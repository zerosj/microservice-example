package com.example.microgateway.definition;


import lombok.Data;

import java.util.List;

/**
 * @author lenovo
 * 网关路由的定义信息
 */

@Data
public class GatewayRouteDefinition {
    private String id ;
    private List<GatewayPredicateDefinition > predicateDefinitions;
    private List<GatewayFilterDefinition> filterDefinitions;
    private String uri;
    private int order = 0;

}
