package com.example.microgateway.service;

import org.springframework.cloud.gateway.route.RouteDefinition;
import reactor.core.publisher.Mono;

/**
 * @author lenovo
 */
public interface DynamicRouteService {
    Mono<String> add(RouteDefinition routeDefinition);
    Mono<String> update(RouteDefinition routeDefinition);
    Mono<String> updateAll(RouteDefinition routeDefinition);
    Mono<String> delete(String routeId);
}
