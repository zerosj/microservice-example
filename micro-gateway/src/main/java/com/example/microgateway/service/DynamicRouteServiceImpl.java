package com.example.microgateway.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.event.RefreshRoutesEvent;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionWriter;
import org.springframework.cloud.gateway.support.NotFoundException;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * @author lenovo
 */
@Component
public class DynamicRouteServiceImpl implements DynamicRouteService,ApplicationEventPublisherAware {
    @Autowired
    private RouteDefinitionWriter routeDefinitionWriter;

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public Mono<String> add(RouteDefinition routeDefinition) {
        routeDefinitionWriter.save(Mono.just(routeDefinition));
        applicationEventPublisher.publishEvent(new RefreshRoutesEvent(routeDefinition));
        return Mono.just("SUCCESS");
    }

    @Override
    public Mono<String> update(RouteDefinition routeDefinition) {
        try {
            routeDefinitionWriter.delete(Mono.just(routeDefinition.getId()));
        } catch (Exception e) {
            e.printStackTrace();
            return Mono.just("update fail,not find route  routeId: "+routeDefinition.getId());
        }
        routeDefinitionWriter.save(Mono.just(routeDefinition));
        applicationEventPublisher.publishEvent(new RefreshRoutesEvent(routeDefinition));
        return Mono.just("SUCCESS");
    }

    @Override
    public Mono<String> updateAll(RouteDefinition routeDefinition) {
        return null;
    }

    @Override
    public Mono<String> delete(String routeId) {
        return routeDefinitionWriter.delete(Mono.just(routeId))
                .then(Mono.defer(() -> {
                    return Mono.just("SUCCESS");
                })).onErrorResume(t ->{
                    return t instanceof NotFoundException;
                }, t ->{
                    return Mono.just("Not Found");
                });
    }
}
