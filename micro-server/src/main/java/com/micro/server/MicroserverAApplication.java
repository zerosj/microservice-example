package com.micro.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.micro.server.remote")
@SpringBootApplication
public class MicroserverAApplication {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","a");
        SpringApplication.run(MicroserverAApplication.class, args);
    }

}
