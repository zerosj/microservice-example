package com.micro.server.config;

import com.micro.server.gray.interceptor.GrayInterceptor;
import com.micro.server.gray.interceptor.GrayRequestInterceptor;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 15:36
 */

@Configuration
public class GrayConfiguration {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new GrayInterceptor());
        return restTemplate;
    }

    @Bean
    public GrayRequestInterceptor grayRequestInterceptor(){
        return new GrayRequestInterceptor();
    }

}
