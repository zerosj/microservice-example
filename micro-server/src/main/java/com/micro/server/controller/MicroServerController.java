package com.micro.server.controller;

import com.micro.server.remote.MicroClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/7 17:25
 */

@RestController
public class MicroServerController {
    @Autowired
    private MicroClient microClient;

    @GetMapping(value = "/hello")
    public String hello(){
        return "Hello, I'm micro server";
    }

    @GetMapping(value = "/hello/{name}")
    public String hello(@PathVariable String name){
        return microClient.hello(name);
    }
}
