package com.micro.server.gray;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 15:25
 */

public class RibbonRequestContext {
    private final Map<String, String> attr = new HashMap<String, String>();

    public String put(String key, String value){
        return attr.put(key, value);
    }

    public String remove(String key){
        return attr.remove(key);
    }

    public String get(String key){
        return attr.get(key);
    }
}
