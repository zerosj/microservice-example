package com.micro.server.gray;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 15:28
 */

public class RibbonRequestContextHolder {
    private static ThreadLocal<RibbonRequestContext> holder = new ThreadLocal<RibbonRequestContext>(){
        @Override
        protected RibbonRequestContext initialValue() {
            return new RibbonRequestContext();
        }
    };

    public static  RibbonRequestContext get(){
        return holder.get();
    }

    public static void setCurrentContext(RibbonRequestContext ribbonRequestContext){
        holder.set(ribbonRequestContext);
    }

    public static void clearContext(){
        holder.remove();
    }
}
