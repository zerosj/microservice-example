package com.micro.server.gray.interceptor;

import com.micro.server.gray.RibbonRequestContext;
import com.micro.server.gray.RibbonRequestContextHolder;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

import static com.micro.server.gray.GrayConstant.GRAY;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 15:32
 */

public class GrayInterceptor implements ClientHttpRequestInterceptor {

    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        if (request.getHeaders().containsKey(GRAY)) {
            String headerValue = request.getHeaders().getFirst(GRAY);
            if (headerValue.equals("true")) {
                RibbonRequestContextHolder.get().put(GRAY,Boolean.TRUE.toString());
            }
        }
        return execution.execute(request, body);
    }
}
