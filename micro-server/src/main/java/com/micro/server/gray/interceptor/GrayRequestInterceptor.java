package com.micro.server.gray.interceptor;

import com.micro.server.gray.RibbonRequestContextHolder;
import feign.RequestInterceptor;
import feign.RequestTemplate;

import static com.micro.server.gray.GrayConstant.GRAY;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 15:41
 */

public class GrayRequestInterceptor implements RequestInterceptor {

    public void apply(RequestTemplate requestTemplate) {
        String headerValue = requestTemplate.headers().get(GRAY).iterator().next();
        if (headerValue.equals(Boolean.TRUE.toString())) {
            RibbonRequestContextHolder.get().put(GRAY,Boolean.TRUE.toString());
        }
    }
}
