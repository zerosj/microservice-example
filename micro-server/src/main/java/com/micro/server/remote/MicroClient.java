package com.micro.server.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(value = "micro-client")
public interface MicroClient {
    @GetMapping(value = "/hello/{name}")
    String hello(@PathVariable(value = "name") String name);
}
