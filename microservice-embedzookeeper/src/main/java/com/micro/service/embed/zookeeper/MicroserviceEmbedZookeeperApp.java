package com.micro.service.embed.zookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/13 20:59
 */

@SpringBootApplication
public class MicroserviceEmbedZookeeperApp {
    public static void main(String[] args) {
        EmbedZookeeperServer.start(2181);
        SpringApplication.run(MicroserviceEmbedZookeeperApp.class, args);
    }
}
