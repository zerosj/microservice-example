package com.micro.service.circuitbreaker;

import java.util.function.Function;
import java.util.function.Supplier;

import static com.micro.service.circuitbreaker.CircuitBreakerState.*;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/9 10:23
 */

public class CircuitBreaker {
    private CircuitBreakerState state;
    private CircuitBreakerConfig config;
    private CircuitBreakerCounter counter;
    private long lastOpenedTime;

    public CircuitBreaker(CircuitBreakerConfig config){
        this.counter = new CircuitBreakerCounter(config.getFailureCount(), config.getFailureTimeInterval());
        this.state = CLOSED;
        this.config = config;
    }

    public <T> T run(Supplier<T> toRun, Function<Throwable, T> fallback){
        try {
            if (state == OPEN) {
                if (halfOpenTimout()){
                    return halfOpenHandle(toRun, fallback);
                }
                return fallback.apply(new DegradeException("degrade circuit breaker"));
            }else if(state == CLOSED){
                T result = toRun.get();
                closed();
                return result;
            }else {
                return halfOpenHandle(toRun, fallback);
            }
        } catch (Exception e) {
            counter.incrFailureCount();
            System.out.println("counter -> "+ counter.getFailureCount());
            if (counter.failureThresholdReached()) {
                //错误次数到达阈值，进入Open状态
                open();
            }
            return fallback.apply(e);
        }
    }

    private <T> T halfOpenHandle(Supplier<T> toRun, Function<Throwable, T> fallback){
        try {
            halfOpen();
            T result = toRun.get();
            int successHalfOpenCount = counter.incrSuccessHalfOpenCount();
            if (successHalfOpenCount >= this.config.getHalfOpenSuccessCount()){
                //HALF-Open状态成功次数到达阈值进入Closed状态
                closed();
            }
            return result;
        }catch (Exception e){
            open();
            return fallback.apply(new DegradeException("degrade by circuit breaker"));
        }
    }

    private boolean halfOpenTimout(){
        return System.currentTimeMillis() - lastOpenedTime > config.getHalfOpenTimeout();
    }

    private void closed(){
        counter.reset();
        state = CLOSED;
    }

    private void open(){
        state = OPEN;
        lastOpenedTime = System.currentTimeMillis();
    }

    private void halfOpen(){
        state = HALF_OPEN;
    }
}
