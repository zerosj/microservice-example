package com.micro.service.circuitbreaker;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 18:46
 */
public class CircuitBreakerConfig {
    //Closed 状态进入Open状态的错误个数阈值
    private int failureCount = 5;

    //failureCount 统计时间窗口
    private long failureTimeInterval = 2 * 10000l;

    //Open状态进入Half-Open状态的超时时间
    private int halfOpenTimeout = 5 * 1000;

    //Half-Open 状态进入 Open状态的成功个数阈值
    private int halfOpenSuccessCount = 2 ;

    public int getFailureCount() {
        return failureCount;
    }

    public void setFailureCount(int failureCount) {
        this.failureCount = failureCount;
    }

    public long getFailureTimeInterval() {
        return failureTimeInterval;
    }

    public void setFailureTimeInterval(long failureTimeInterval) {
        this.failureTimeInterval = failureTimeInterval;
    }

    public int getHalfOpenTimeout() {
        return halfOpenTimeout;
    }

    public void setHalfOpenTimeout(int halfOpenTimeout) {
        this.halfOpenTimeout = halfOpenTimeout;
    }

    public int getHalfOpenSuccessCount() {
        return halfOpenSuccessCount;
    }

    public void setHalfOpenSuccessCount(int halfOpenSuccessCount) {
        this.halfOpenSuccessCount = halfOpenSuccessCount;
    }
}
