package com.micro.service.circuitbreaker;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 18:54
 */

public class CircuitBreakerCounter {
    //Closed 状态进入Open状态的错误个数阈值
    private int failureCount = 5;

    //failureCount 统计时间窗口
    private long failureTimeInterval = 2 * 100000l;

    //当前错误次数
    private AtomicInteger failureCurrentCount;

    //上一次调用失败时间戳
    private long lastFailureTime;

    //Half-Open 状态下成功次数
    private final AtomicInteger halfOpenSuccessCount;

    public CircuitBreakerCounter(int failureCount, long failureTimeInterval) {
        this.failureCount = failureCount;
        this.failureTimeInterval = failureTimeInterval;
        this.failureCurrentCount = new AtomicInteger(0);
        this.lastFailureTime = System.currentTimeMillis();
        this.halfOpenSuccessCount = new AtomicInteger(0);
    }

    public synchronized int incrFailureCount(){
        long current = System.currentTimeMillis();
        if ((current - lastFailureTime > failureTimeInterval)) {
            //超过时间窗口当前失败次数重置为0
            lastFailureTime = current;
            failureCurrentCount.set(0);
        }
        System.out.println("failureCurrentCount ->"+failureCurrentCount.get());
        return failureCurrentCount.getAndIncrement();
    }

    public int incrSuccessHalfOpenCount(){
        return this.halfOpenSuccessCount.getAndIncrement();
    }

    public boolean failureThresholdReached(){
        return getFailureCount() > failureCount;
    }

    public int getFailureCount(){
        return failureCurrentCount.get();
    }

    public void reset(){
        halfOpenSuccessCount.set(0);
        failureCurrentCount.set(0);
    }

}
