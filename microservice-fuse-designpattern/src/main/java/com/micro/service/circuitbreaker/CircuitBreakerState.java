package com.micro.service.circuitbreaker;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 18:44
 */

public enum CircuitBreakerState {
    CLOSED,
    HALF_OPEN,
    OPEN,
}
