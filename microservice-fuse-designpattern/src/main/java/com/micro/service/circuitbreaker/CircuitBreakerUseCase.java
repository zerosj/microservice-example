package com.micro.service.circuitbreaker;

import org.springframework.web.client.RestTemplate;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/9 10:50
 */

public class CircuitBreakerUseCase {
    public static void main(String[] args) {
//        CircuitBreaker circuitBreaker = new CircuitBreaker(new CircuitBreakerConfig());
//        String run = circuitBreaker.run(() -> {
//            return "deep in spring cloud";
//        }, t -> {
//            return "boom";
//        });

        CircuitBreaker breaker = new CircuitBreaker(new CircuitBreakerConfig());
        RestTemplate restTemplate = new RestTemplate();
//        String result = breaker.run(() -> {
//            return restTemplate.getForObject("https://httpbin.org/status/500", String.class);
//
//        }, t -> {
//            return "boom";
//        });
//        System.out.println(result);

        int degradeCount = 0;
        for (int index = 0; index < 10; index ++){
            String result = breaker.run(() -> {
                return restTemplate.getForObject("https://httpbin.org/status/500", String.class);
            }, t -> {
                if (t instanceof DegradeException) {
                    return "degrade";
                }
                return "boom";
            });
            System.out.println("result ->" +result);

            if (result.equals("degrade")) {
                degradeCount ++;
            }
        }
        System.out.println(degradeCount);
    }
}
