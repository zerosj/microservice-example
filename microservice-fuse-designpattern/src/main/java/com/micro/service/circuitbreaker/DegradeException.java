package com.micro.service.circuitbreaker;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/9 10:39
 */

public class DegradeException extends RuntimeException{

    public DegradeException(String message){
        super(message);
    }

    public DegradeException(String message, Throwable cause){
        super(message, cause);
    }

    public DegradeException(Throwable cause){
        super(cause);
    }
}
