package com.micro.service.circuitbreaker;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/9 11:52
 */

public class Main {

    public static void main(String[] args) {
        AtomicInteger counter = new AtomicInteger(0);
        for (int i = 0; i < 10; i++) {
            System.out.println(counter.getAndIncrement());
        }
    }
}
