package com.micro.service.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/13 18:46
 */

@EnableHystrixDashboard
@SpringBootApplication
public class MicroserviceDashboardApp {
    public static void main(String[] args) {
        SpringApplication.run(MicroserviceDashboardApp.class, args);
    }
}
