package com.micro.service.hystrix.example;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/13 18:31
 */


@EnableFeignClients
@EnableHystrix
@SpringCloudApplication
public class MicroserviceHystrixApp {
    public static void main(String[] args) {
        SpringApplication.run(MicroserviceHystrixApp.class, args);
    }
}
