package com.micro.service.hystrix.example.controller;

import com.micro.service.hystrix.example.remote.MicroServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/13 18:39
 */

@RestController
public class MicroserviceHystrixController {
    @Autowired
    private MicroServer microServer;

    @GetMapping(value = "/get")
    public String get(){
        return microServer.hello();
    }
}
