package com.micro.service.hystrix.example.remote;

import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "micro-server",fallbackFactory = MicroServer.MicroServerFallbackFactory.class)
public interface MicroServer {
    @GetMapping(value = "/hello")
    String hello ();

    @Component
    public static class MicroServerFallbackFactory implements FallbackFactory<MicroServer> {
        public MicroServer create(Throwable cause) {
            return new MicroServer() {
                public String hello() {
                    return "fallback";
                }
            };
        }
    }
}
