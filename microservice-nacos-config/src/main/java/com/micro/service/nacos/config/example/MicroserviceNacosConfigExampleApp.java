package com.micro.service.nacos.config.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 17:54
 */

@SpringBootApplication
public class MicroserviceNacosConfigExampleApp {

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceNacosConfigExampleApp.class, args);
    }
}
