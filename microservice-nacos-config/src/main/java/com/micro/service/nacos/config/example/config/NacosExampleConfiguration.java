package com.micro.service.nacos.config.example.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 18:32
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "nacos.example")
public class NacosExampleConfiguration {
    private String name;
    private String addr;
    private Map<String,String> attr;
    private List<String> attrs;


    @Override
    public String toString() {
        return "NacosExampleConfiguration{" +
                "name='" + name + '\'' +
                ", addr='" + addr + '\'' +
                ", attr=" + attr +
                ", attrs=" + attrs +
                '}';
    }
}
