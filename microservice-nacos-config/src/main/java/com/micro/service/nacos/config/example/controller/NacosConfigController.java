package com.micro.service.nacos.config.example.controller;

import com.micro.service.nacos.config.example.config.NacosExampleConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 18:08
 */
@RefreshScope
@RestController
public class NacosConfigController {
    @Value("${username}")
    private String username;

    @Autowired
    private NacosExampleConfiguration configuration;

    @GetMapping("/get")
    public String get(){
        return username;
    }

    @GetMapping(value = "/getConfig")
    public String getConfig(){
        return configuration.toString();
    }
}
