package com.micro.service.distributed.lock;

import com.micro.service.distributed.lock.aspect.RedissonLockAspectSupport;
import com.micro.service.distributed.lock.factory.DistributedLockFactory;
import com.micro.service.distributed.lock.service.DistributedLockServiceWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/10 10:12
 */

@SpringBootApplication
public class MicroserviceRedisLockApp {
    public static void main(String[] args) {
        SpringApplication.run(MicroserviceRedisLockApp.class, args);
    }

    @Bean
    public RedissonLockAspectSupport redissonLockAspectSupport(){
        return new RedissonLockAspectSupport();
    }

    @Bean
    public DistributedLockFactory distributedLockFactory(){
        return new DistributedLockFactory();
    }

    @Bean
    public DistributedLockServiceWorker distributedLockServiceWorker(){
        return new DistributedLockServiceWorker(distributedLockFactory());
    }
}
