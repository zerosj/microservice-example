package com.micro.service.distributed.lock.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/10 10:12
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DistributedLock {
    boolean fair() default true;
    int expireTime() default 10;
    TimeUnit timeUnit() default TimeUnit.SECONDS;
    String key();

}
