package com.micro.service.distributed.lock.aspect;

import com.micro.service.distributed.lock.annotation.DistributedLock;
import com.micro.service.distributed.lock.service.DistributedLockServiceWorker;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Method;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/10 10:48
 */
@Slf4j
@Aspect
public class RedissonLockAspectSupport {
    @Autowired
    private DistributedLockServiceWorker distributedLockServiceWorker;

    @Pointcut(value = "@annotation(com.micro.service.distributed.lock.annotation.DistributedLock)")
    public void pointcut(){

    }

    @Around("pointcut()")
    public Object process(ProceedingJoinPoint joinPoint){
        Object result = null;
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        DistributedLock distributedLock = method.getAnnotation(DistributedLock.class);
        if (distributedLock != null) {
            log.info("......prepare to get lock");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean acquire = distributedLockServiceWorker.acquire(distributedLock.fair(), distributedLock.key(), distributedLock.expireTime(), distributedLock.timeUnit());
            log.info("lock.status......{}",acquire);
            if (acquire){
                try {
                   result = joinPoint.proceed();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }finally {
                    log.info("......start release lock");
                    boolean release = distributedLockServiceWorker.release(true, true, distributedLock.key(), distributedLock.expireTime(), distributedLock.timeUnit());
                    log.info("lock.status......{}",release);
                }
            }
        }

        return result;
    }
}
