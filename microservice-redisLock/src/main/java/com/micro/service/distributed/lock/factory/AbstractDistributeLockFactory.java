package com.micro.service.distributed.lock.factory;

import org.redisson.api.RLock;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/10 12:01
 */

public abstract class AbstractDistributeLockFactory {
    public abstract RLock createLock(String key, boolean fair);

}
