package com.micro.service.distributed.lock.factory;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/10 12:03
 */

public class DistributedLockFactory extends AbstractDistributeLockFactory{
    @Autowired
    private RedissonClient redissonClient;

    @Override
    public RLock createLock(String key, boolean fair) {
        if (fair) {
            return redissonClient.getFairLock(key);
        }else {
            return redissonClient.getLock(key);
        }
    }
}
