package com.micro.service.distributed.lock.service;

import com.micro.service.distributed.lock.annotation.DistributedLock;
import org.springframework.stereotype.Service;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/10 11:00
 */

@Service
public class CaculatorService {
    private  int val = 0;
    @DistributedLock(key = "getValue")
    public void getValue(){
        val++ ;
        System.out.println(val);
    }
}
