package com.micro.service.distributed.lock.service;

import java.util.concurrent.TimeUnit;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2050</p>
 *
 * @author Rotten
 * @version 1.0
 * @date
 */

public interface DistributedLockService {
    boolean acquire(boolean fair,String key, long expireTime, TimeUnit timeUnit);
    boolean release(boolean force, boolean async, String key,long expireTime, TimeUnit timeUnit);
}
