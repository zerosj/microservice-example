package com.micro.service.distributed.lock.service;


import com.micro.service.distributed.lock.factory.DistributedLockFactory;
import org.redisson.api.RLock;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/10 11:18
 */
public class DistributedLockServiceWorker implements DistributedLockService {
    private final DistributedLockFactory distributedLockFactory;
    private RLock lock;

    public DistributedLockServiceWorker(DistributedLockFactory distributedLockFactory){
        this.distributedLockFactory = distributedLockFactory;
    }

    public DistributedLockServiceWorker(DistributedLockFactory distributedLockFactory, String key, boolean fair){
        this.distributedLockFactory = distributedLockFactory;
        this.lock = distributedLockFactory.createLock(key, fair);
    }

    @Override
    public boolean acquire(boolean fair, String key, long expireTime, TimeUnit timeUnit) {
        if (lock == null) {
            this.lock = distributedLockFactory.createLock(key, fair);
        }
        try {
            lock.lock(expireTime, timeUnit);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean release(boolean force, boolean async, String key, long expireTime, TimeUnit timeUnit) {
        if (ifAbleRelease(lock)) {
            if (force&& async) {
                try {
                    return lock.forceUnlockAsync().get(expireTime,timeUnit);
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    e.printStackTrace();
                    return false;
                }
            }else if (force){
                try {
                    return lock.forceUnlock();
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            } else {
                lock.unlock();
                return true;
            }
        }
        return false;
    }

    private boolean ifAbleRelease(RLock lock){
        return lock.isLocked() && lock.isHeldByCurrentThread();
    }
}
