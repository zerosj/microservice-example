package com.micro.service.distributed.lock;

import com.micro.service.distributed.lock.service.CaculatorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/10 10:57
 */

@SpringBootTest
public class MicroserviceRedisLockAppTests {
    @Autowired
    private CaculatorService caculatorService;

    @Test
    void redisLockTest(){
        for (int i = 0; i < 10; i++) {
            new Thread(() ->{
                caculatorService.getValue();
            }).run();
        }

    }
}
