package com.micro.service.sac;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/12 12:10
 */

@EnableWebSecurity
@SpringBootApplication
public class MicroserviceSacApp {
    public static void main(String[] args) {
        SpringApplication.run(MicroserviceSacApp.class, args);
    }

}
