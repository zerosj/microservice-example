package com.micro.sentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 11:14
 */

@SentinelResource
@SpringBootApplication
public class MicroserviceSentinelExampleApp {

    public static void main(String[] args) {
        new SpringApplicationBuilder(MicroserviceSentinelExampleApp.class).run(args);
    }
}
