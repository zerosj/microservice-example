package com.micro.sentinel.configuration;

import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 11:44
 */

@Configuration
public class SentinelAspectConfiguration {

    @Bean
    public SentinelResourceAspect sentinelResourceAspect(){
        return new SentinelResourceAspect();
    }
}
