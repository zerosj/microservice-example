package com.micro.sentinel.controller;

import com.alibaba.cloud.sentinel.annotation.SentinelRestTemplate;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.micro.sentinel.service.SentinelFlowService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 11:13
 */

@RestController
public class SentinelFlowController {

    @SentinelResource(value = "/qpslimit",blockHandler = "qpsLimitBlockHandler")
    @GetMapping(value = "/qpslimit")
    public String qpsLimit(){
        return "qpsLimit限流方法";
    }

    @SentinelResource(value = "/tnl",fallback = "threadNumLimitFallback")
    @GetMapping(value = "/tnl")
    public String threadNumLimit(){
        return "线程数限流方法";
    }

    public String qpsLimitBlockHandler(BlockException blockException) {
        blockException.printStackTrace();
        return "qps limit block handler";
    }

    public String qpsLimitFallback() {
        return "qps limit fallback";
    }

    public String threadNumLimitFallback() {
        return "thread num limit fallback";
    }

}
