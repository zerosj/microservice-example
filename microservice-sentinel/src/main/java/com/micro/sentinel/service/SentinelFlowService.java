package com.micro.sentinel.service;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/8 11:51
 */

public interface SentinelFlowService {
    String qpsLimit();
    String threadNumLimit();
}
