package com.micro.service.stream.example;

import com.micro.service.stream.example.channel.MyMessageChannel;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/13 20:44
 */

@RestController
@EnableBinding(MyMessageChannel.class)
@SpringBootApplication
public class MicroserviceStreamApp {
    public static void main(String[] args) {
       new SpringApplicationBuilder().sources(MicroserviceStreamApp.class)
       .run(args);
    }

}
