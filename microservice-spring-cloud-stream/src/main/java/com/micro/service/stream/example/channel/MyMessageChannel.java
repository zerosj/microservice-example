package com.micro.service.stream.example.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/15 13:34
 */

public interface MyMessageChannel {
    /**
     * 发消息的通道
     *
     * @return
     */
    @Output(TOPIC.OUTPUT)
    MessageChannel messageChannel();

    /**
     * 收消息的通道
     *
     * @return
     */
    @Input(TOPIC.INPUT)
    SubscribableChannel subscribeChannel();

    public static class TOPIC{
        public static final String OUTPUT = "output-topic";
        public static final String INPUT = "input-topic";
    }
}
