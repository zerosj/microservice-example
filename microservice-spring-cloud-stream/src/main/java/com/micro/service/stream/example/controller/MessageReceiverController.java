package com.micro.service.stream.example.controller;

import com.micro.service.stream.example.channel.MyMessageChannel;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/15 13:09
 */

@RestController
public class MessageReceiverController {

    @StreamListener(MyMessageChannel.TOPIC.INPUT)
    public void receive(Message<String> message){
        System.out.println("message..................."+message.getPayload());
    }
}
