package com.micro.service.stream.example.controller;

import com.micro.service.stream.example.channel.MyMessageChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/15 13:10
 */

@RestController
public class MessageSenderController {
    @Autowired
    private MyMessageChannel messageChannel;

    @PostMapping(value = "/send")
    public String send(){
        Boolean send = messageChannel.messageChannel().send(MessageBuilder.withPayload("hello world")
                .setHeader("k1", "v1").build());
        return send.toString();
    }
}
