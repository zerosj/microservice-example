package com.micro.service.admin.example;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>Title: MyDiscovery</p>
 * <p>Description: MyDiscovery</p>
 * <p>Copyright: Copyright (c) 2021-2055</p>
 *
 * @author lenovo
 * @version 1.0
 * @date 2021/6/9 16:16
 */
@EnableAdminServer
@SpringBootApplication
public class MicroSpringBootAdminApp {
    public static void main(String[] args) {
        SpringApplication.run(MicroSpringBootAdminApp.class, args);
    }
}
